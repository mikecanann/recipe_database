
import React, { Component } from 'react';


class CreateEditForm extends Component {

  constructor() {
    super();

    this.state = {
      name: '',
      ingredients: '',
      instructions: '',
      created: false,
    };
  }

  componentDidMount() {
    this.setStateFromRecipe(this.props.recipe);
  }
  componentWillReceiveProps(nextProps) {
    this.setStateFromRecipe(nextProps.recipe);
  }

  setStateFromRecipe(recipe) {
    this.setState({
      name: recipe ? recipe.name : '',
      ingredients: recipe ? recipe.ingredients : '',
      instructions: recipe ? recipe.instructions : '',
    });
  }

  handleChangeName(event) {
    this.setState({name: event.target.value});
  }
  handleChangeIngredients(event) {
    this.setState({ingredients: event.target.value});
  }
  handleChangeInstructions(event) {
    this.setState({instructions: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();

    const {name, ingredients, instructions} = this.state;

    if(this.props.recipe) {
      this.props.onEdit(name, ingredients, instructions);
    }
    else {

      this.props.onCreate(name, ingredients, instructions);


      this.resetForm();
      this.setState({ created: true});
      this.refs.name.focus();
    }
  }

  resetForm() {
    this.setState({
      name: '',
      ingredients: '',
      instructions: ''
    });
  }

  render () {

    const {name, ingredients, instructions, created} = this.state;
    const {recipe} = this.props;
    return (

      <form onSubmit={this.handleSubmit.bind(this)}>

        { created && <div className='alert alert-success'> recipe created </div>}

        <div className='form-group'>
          <label htmlFor='name'> Recipe name:</label>
          <input
            className='form-control'
            rows='5'
            id='name'
            placeholder='Enter recipe name ...'
            style={{
              width: '100%',
            }}
            value={name}
            onChange={this.handleChangeName.bind(this)}
            ref='name'
          />
        </div>

        <div className='form-group'>
          <label htmlFor='ingredients'> Ingredients:</label>
          <textarea
            className='form-control'
            rows='5'
            id='ingredients'
            placeholder='Enter ingredients ...'
            style={{
              width: '100%',
            }}
            value={ingredients}
            onChange={this.handleChangeIngredients.bind(this)}
          />
        </div>

        <div className='form-group'>
          <label htmlFor='instructions'> Instructions:</label>
          <textarea
            className='form-control'
            rows='10'
            id='instructions'
            placeholder='Enter instructions ...'
            style={{
              width: '100%',
            }}
            value={instructions}
            onChange={this.handleChangeInstructions.bind(this)}
          />
        </div>

        <input
          className='btn btn-default'
          type='submit'
          value={recipe ? 'Edit' : 'Create'}
        />

      </form>
  );
  }
};

CreateEditForm.propTypes = {
  onCreate: React.PropTypes.func.isRequired,
  onEdit: React.PropTypes.func.isRequired,
  recipe: React.PropTypes.object,
};

export default CreateEditForm;
